package by.gsu.epam.model.beans;


public class User {
    private String name;
    private Role role;
    private String password;
    public User() {
        super();
    }
    public User(String name, String password,Role role) {
        super();
        this.name = name;
        this.password = password;
        this.role = role;
    }
    public User(String name, String password) {
        super();
        this.name = name;
        this.password = password;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }
    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
