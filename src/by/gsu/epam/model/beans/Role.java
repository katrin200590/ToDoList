package by.gsu.epam.model.beans;

public enum Role {
    ADMIN, USER, GUEST
}
