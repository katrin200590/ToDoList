package by.gsu.epam.model.utils;

import by.gsu.epam.model.exception.DaoException;

import java.sql.*;
import java.util.ResourceBundle;

public class  ConnectorDB {

    private Connection connection;

    public Connection connect() throws DaoException {
        ResourceBundle resource = ResourceBundle.getBundle(Constants.ADMIN_PROPERTY);
        String url = resource.getString(Constants.DB_URL);
        String user = resource.getString(Constants.DB_USER);
        String pass = resource.getString(Constants.DB_PASSWORD);
        String driver = resource.getString(Constants.DB_DRIVER);
        if (connection == null) {
            try {
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, pass);
            } catch (ClassNotFoundException | SQLException e) {
                throw new DaoException(Constants.PROBLEM_DB, e);

            }
        }
        return connection;
    }

    public  void closeConnection() throws DaoException{
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                throw new DaoException(Constants.CLOSING_PROBLEM, e);
            }
        }
    }

    public static void closeResultSets(ResultSet...resultSets) throws DaoException {
        for(ResultSet resultSet : resultSets) {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    throw new DaoException(Constants.CLOSING_PROBLEM, e);
                }
            }
        }
    }

    public static void closeStatements(Statement...statements) throws  DaoException {
        for(Statement statement : statements) {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    throw new DaoException(Constants.CLOSING_PROBLEM, e);

                }
            }
        }
    }
}