package by.gsu.epam.model.utils;

public class Constants {
    public static final String JUMP_ERROR = "/index.jsp";
    public static final String JUMP_LOGIN = "/views/login.jsp";
    public static final String JUMP_REGISTRATION = "/views/singup.jsp";
    public static final String JUMP_USER = "/views/user.jsp";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_USER = "user";
    public static final String ADMIN_PROPERTY = "admin";
    public static final String ADMIN_CSV = "csvFile";
    public static final String CLASSES_REAL_PATH ="C:/Users/XXX/Desktop/Epam Java/WebJ2EE/ToDo";
    public static final String DB_URL = "db.url";
    public static final String DB_USER = "db.user";
    public static final String DB_PASSWORD = "db.password";
    public static final String DB_DRIVER = "db.driver";
    public static final String ERROR_SOURCE_CSV = "Error with reading CSV";
    public static final String ERROR_SOURCE_CSV2 = "Error with writing CSV";
    public static final String PROBLEM_DB = "Problem with DB";
    public static final String CLOSING_PROBLEM= "Resource closing problem";
    public static final int LOGIN_INDEX = 1;
    public static final int PASSWORD_INDEX = 2;
    public static final String SELECT_LOGIN_PASSWORD = "SELECT users.login, users.password FROM users";
    public static final String INSERT_LOGIN_PASSWORD = "INSERT INTO users (login,password) VALUES(?,?)";
    public static final String SELECT_LOGIN_PARAM ="SELECT users.login FROM users WHERE (users.login=?)";
    public static final String SELECT_PASSWORD_PARAM ="SELECT users.password FROM users WHERE (users.password=?)";
    public static final String KEY_ERROR = "errMessage";
    public static final String ERROR_LOG_IN ="!Wrong login or password!";
    public static final String ERROR_SING_UP ="!Such an account or password exists!";

}
