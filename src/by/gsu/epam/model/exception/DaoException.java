package by.gsu.epam.model.exception;

public class DaoException extends Exception {
    private String error;


    public DaoException(String error, Exception e) {
        super(e);
        this.error = error;

    }

    @Override
    public String toString() {
        return error + ":" +super.getMessage();
    }

    @Override
    public String getMessage() {
        return error;
    }

}
