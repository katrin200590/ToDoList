package by.gsu.epam.model.dao;

import by.gsu.epam.model.utils.Constants;
import by.gsu.epam.model.exception.DaoException;
import by.gsu.epam.model.beans.Role;
import by.gsu.epam.model.beans.User;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.Scanner;

public class CsvUserImpl implements IUserDAO {
    private final String CSV_SPLITTER = ";";
    private final int LOGIN_NUM = 0, PASSWORD_NUM = 1;
    private final ResourceBundle resource = ResourceBundle.getBundle(Constants.ADMIN_PROPERTY);
    private final String fileName =Constants.CLASSES_REAL_PATH+ resource.getString(Constants.ADMIN_CSV);
    private final String ADMIN_LOGIN = resource.getString(Constants.KEY_LOGIN);
    private final String ADMIN_PASSWORD = resource.getString(Constants.KEY_PASSWORD);

    @Override
    public User getUser(String login, String password) throws DaoException {

        Scanner sc = null;
        try {
            Role role = Role.GUEST;
            sc = new Scanner(new FileReader(fileName));
            while (sc.hasNext()) {
                String[] str = sc.next().split(CSV_SPLITTER);
                if (str[LOGIN_NUM].equals(login) && str[PASSWORD_NUM].equals(password)) {
                    role = Role.USER;
                    break;
                }
            }
            if(ADMIN_LOGIN.equals(login) && ADMIN_PASSWORD.equals(password)) {
                role = Role.ADMIN;
            }
            return new User(login, password, role);
        } catch (IOException e) {
            throw new DaoException(Constants.ERROR_SOURCE_CSV, e);
        } finally {
            if (sc != null) {
                sc.close();
            }
        }
    }

    @Override
    public void addUser(String login, String password) throws DaoException {

        try {
            FileWriter writer = new FileWriter(fileName, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            String text = '\n' + login + ";" + password;
            bufferWriter.write(text);
            bufferWriter.close();
        } catch (IOException e) {
            throw new DaoException(Constants.ERROR_SOURCE_CSV2, e);
        }
    }

    @Override
    public boolean isUserExist(String login, String password) throws DaoException {

        Scanner sc = null;
        boolean flag=false;
        if(ADMIN_LOGIN.equals(login) | ADMIN_PASSWORD.equals(password)) {
            flag = true;
        }
        try {
            sc = new Scanner(new FileReader(fileName));
            while (sc.hasNext()) {
                String[] str = sc.next().split(CSV_SPLITTER);
                if (str[LOGIN_NUM].equals(login) | str[PASSWORD_NUM].equals(password)) {
                    flag=true;
                    break;
                }
            }

        } catch (IOException e) {
            throw new DaoException(Constants.ERROR_SOURCE_CSV, e);

        } finally {
            if (sc != null) {
                sc.close();
            }
        }
        return flag;
    }
}



