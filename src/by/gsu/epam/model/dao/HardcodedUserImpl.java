package by.gsu.epam.model.dao;

import by.gsu.epam.model.beans.UsersLogins;
import by.gsu.epam.model.utils.Constants;
import by.gsu.epam.model.beans.Role;
import by.gsu.epam.model.beans.User;
import java.util.Map;
import java.util.ResourceBundle;

public class HardcodedUserImpl implements IUserDAO {
    private ResourceBundle resource = ResourceBundle.getBundle(Constants.ADMIN_PROPERTY);
    private final String ADMIN_LOGIN = resource.getString(Constants.KEY_LOGIN);
    private final String ADMIN_PASSWORD = resource.getString(Constants.KEY_PASSWORD);

    public User getUser(String login, String password){
        Role role;
        if(ADMIN_LOGIN.equals(login) && ADMIN_PASSWORD.equals(password)) {
            role = Role.ADMIN;
        }
        else if (isUserExist(login, password)) {
            role = Role.USER;
        }
        else role = Role.GUEST;

        return new User(login, password, role);
    }

    @Override
    public void addUser(String login, String password) {
        UsersLogins.mapOfUsers.put(login, new User(login, password));

    }

    @Override
    public boolean isUserExist(String login, String password) {
        boolean flag=false;
        if(ADMIN_LOGIN.equals(login) | ADMIN_PASSWORD.equals(password)) {
            flag = true;
        }
        for (Map.Entry entry : UsersLogins.mapOfUsers.entrySet()) {

         if (entry.getKey().equals(login) | ((User)entry.getValue()).getPassword().equals(password)) {
                flag = true;
            }
        }
        return flag;
    }

}
