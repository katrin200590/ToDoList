package by.gsu.epam.model.dao;

public class UserFactory {

        public static IUserDAO getClassFromFactory(String userImpl) {
            IUserDAO iUserDAO = null;

            switch (userImpl) {
                case "hardCode":
                    iUserDAO = new HardcodedUserImpl();
                    break;
                case "csvFile":
                    iUserDAO = new CsvUserImpl();
                    break;
                case "dataBase":
                    iUserDAO = new DataBaseImpl();
                    break;
            }

                return iUserDAO;
        }
}

