package by.gsu.epam.model.dao;

import by.gsu.epam.model.beans.Role;
import by.gsu.epam.model.beans.User;
import by.gsu.epam.model.exception.DaoException;
import by.gsu.epam.model.utils.ConnectorDB;
import by.gsu.epam.model.utils.Constants;
import java.sql.*;
import java.util.ResourceBundle;

public class DataBaseImpl implements IUserDAO {
    private ResourceBundle resource = ResourceBundle.getBundle(Constants.ADMIN_PROPERTY);
   private final String ADMIN_LOGIN = resource.getString(Constants.KEY_LOGIN);
    private final String ADMIN_PASSWORD = resource.getString(Constants.KEY_PASSWORD);

    @Override
    public User getUser(String login, String password) throws DaoException {


        ConnectorDB connectorDB=new ConnectorDB();
        Statement st=null;
        ResultSet rs = null;

        try {
            Role role = Role.GUEST;

            if (ADMIN_LOGIN.equals(login) && ADMIN_PASSWORD.equals(password)) {
                role = Role.ADMIN;
            }
            else {

                st = connectorDB.connect().createStatement();
                rs = st.executeQuery(Constants.SELECT_LOGIN_PASSWORD);
                while (rs.next()) {
                    if (rs.getString(Constants.LOGIN_INDEX).equals(login) && rs.getString(Constants.PASSWORD_INDEX).equals(password)) {
                        role = Role.USER;
                        break;
                    }
                }
            }

            return new User(login,password, role);
        } catch (SQLException e) {
            throw new DaoException(Constants.PROBLEM_DB, e);
        } finally {
            ConnectorDB.closeResultSets(rs);
            ConnectorDB.closeStatements(st);
            connectorDB.closeConnection();
        }
    }

    @Override
    public void addUser(String login, String password) throws DaoException {

        ConnectorDB connectorDB=new ConnectorDB();
        PreparedStatement ps = null;

        try {
            ps = connectorDB.connect().prepareStatement(Constants.INSERT_LOGIN_PASSWORD);
            ps.setString(Constants.LOGIN_INDEX, login);
            ps.setString(Constants.PASSWORD_INDEX, password);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException(Constants.PROBLEM_DB, e);
        } finally {
            ConnectorDB.closeStatements(ps);
           connectorDB.closeConnection();
           }
    }
    public boolean isUserExist (String login, String password) throws DaoException {
        ConnectorDB connectorDB = new ConnectorDB();
        PreparedStatement psSelectLogin = null;
        PreparedStatement psSelectPassword = null;

        boolean flag;

        try {
            psSelectLogin = connectorDB.connect().prepareStatement(Constants.SELECT_LOGIN_PARAM);
            psSelectLogin.setString(Constants.LOGIN_INDEX, login);
            ResultSet rsLogin = psSelectLogin.executeQuery();
            flag = rsLogin.next();
            if (!flag) {
                psSelectPassword = connectorDB.connect().prepareStatement(Constants.SELECT_PASSWORD_PARAM);
                psSelectPassword.setString(Constants.LOGIN_INDEX, password);
                ResultSet rsPassword = psSelectPassword.executeQuery();
                flag = rsPassword.next();
            }

        } catch (SQLException e) {
            throw new DaoException(Constants.PROBLEM_DB, e);
        } finally {
            ConnectorDB.closeStatements(psSelectLogin,psSelectPassword);
            connectorDB.closeConnection();

        }
        if(ADMIN_LOGIN.equals(login) | ADMIN_PASSWORD.equals(password)) {
            flag = true;
        }
        return flag;

    }
}