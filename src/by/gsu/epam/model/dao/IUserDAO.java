package by.gsu.epam.model.dao;

import by.gsu.epam.model.exception.DaoException;
import by.gsu.epam.model.beans.User;

public interface IUserDAO {
     User getUser(String login, String password) throws DaoException;
     void addUser(String login, String password) throws DaoException;
     boolean isUserExist(String login, String password) throws DaoException;
}


