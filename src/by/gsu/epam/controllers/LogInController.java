package by.gsu.epam.controllers;

import by.gsu.epam.model.beans.Role;
import by.gsu.epam.model.beans.User;
import by.gsu.epam.model.dao.IUserDAO;
import by.gsu.epam.model.dao.UserFactory;
import by.gsu.epam.model.exception.DaoException;
import by.gsu.epam.model.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogInController extends AbstractController {

    protected void performTask(HttpServletRequest request,
                               HttpServletResponse response) throws ServletException, IOException {


        String userImpl = getServletContext().getInitParameter("userImpl");
        IUserDAO userDAO = UserFactory.getClassFromFactory(userImpl);
        String login=request.getParameter(Constants.KEY_LOGIN),
                password=request.getParameter(Constants.KEY_PASSWORD);

        try {

            User user = userDAO.getUser(login, password);
            if (user.getRole() == Role.GUEST)
            {
                request.setAttribute(Constants.KEY_ERROR, Constants.ERROR_LOG_IN);
                jump(Constants.JUMP_LOGIN, request, response);
            }

            request.setAttribute(Constants.KEY_USER, user);

            jump(Constants.JUMP_USER, request, response);
        } catch (DaoException e) {
            e.printStackTrace();
            jumpError(e.getMessage(), request, response);
        }
    }
}
