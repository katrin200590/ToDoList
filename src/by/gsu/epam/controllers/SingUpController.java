package by.gsu.epam.controllers;

import by.gsu.epam.model.dao.IUserDAO;
import by.gsu.epam.model.dao.UserFactory;
import by.gsu.epam.model.exception.DaoException;
import by.gsu.epam.model.utils.Constants;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SingUpController extends AbstractController {

    protected void performTask(HttpServletRequest request,
                               HttpServletResponse response) throws ServletException, IOException {

        String login=request.getParameter(Constants.KEY_LOGIN),
                password=request.getParameter(Constants.KEY_PASSWORD);

        String userImpl = getServletContext().getInitParameter("userImpl");
        IUserDAO userDAO = UserFactory.getClassFromFactory(userImpl);
        synchronized (this) {
            try {
                if (userDAO.isUserExist(login, password)) {
                    request.setAttribute(Constants.KEY_ERROR, Constants.ERROR_SING_UP);
                    jump(Constants.JUMP_REGISTRATION, request, response);
                } else {
                    userDAO.addUser(login, password);
                    jump(Constants.JUMP_LOGIN, request, response);
                }
            } catch (DaoException e) {
                e.printStackTrace();
                jumpError(e.getMessage(), request, response);
            }
        }

    }
}
