package by.gsu.epam.controllers;

import by.gsu.epam.model.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StartSingup extends AbstractController {

    protected void performTask(HttpServletRequest request,
                               HttpServletResponse response) throws ServletException, IOException {

        jump(Constants.JUMP_REGISTRATION, request, response);

    }
}