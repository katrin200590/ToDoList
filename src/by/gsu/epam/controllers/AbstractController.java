package by.gsu.epam.controllers;

import by.gsu.epam.model.utils.Constants;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractController extends HttpServlet {
    abstract void performTask (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

        public void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        performTask(request, response);
    }

        public void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        performTask(request, response);
    }

        void jump(String url, HttpServletRequest request,
                  HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(request, response);
    }
        void jumpError(String message, HttpServletRequest request,
                       HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(Constants.KEY_ERROR, message);
        RequestDispatcher rd = getServletContext().getRequestDispatcher(Constants.JUMP_ERROR);
        rd.forward(request, response);
    }



}
