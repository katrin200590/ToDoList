<%@ page import="by.gsu.epam.model.utils.Constants" %>
<%--
  Created by IntelliJ IDEA.
  User: XXX
  Date: 05.02.2018
  Time: 12:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
  <title>Start</title>
  <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/reset.css" type="text/css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/layout.css" type="text/css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main-page.css" type="text/css">



</head>
<body>
<div class="header">
<div class="logo-image">
  <img src="${pageContext.request.contextPath}/image/to-do-list-logo.png">
  <h2 class="logo-text">My ToDoList</h2>
</div>

  <div class="menu">
  <a href="startLogin">Log in</a>
  <a href="startSingup">Sign up</a>




</div>


</div>


<div class="content">

  <div class="content-text">
    <c:if test="${not empty errMessage}">
      <div class="err_message"><c:out value="${errMessage}" /></div>
    </c:if>
    <h2 class="content-header">Information about project</h2>
    <p>The application is a ToDo list with the ability to attach a file to each task of the list. The list should contain three sections: Today, Tomorrow and Someday. Each of the sections can contain an unlimited number of tasks.</p>
    <a href="startLogin">Log in</a>
    <a href="startSingup">Sign up</a>


  </div>
  <img class="big-image" src="${pageContext.request.contextPath}/image/main-page.png">

</div>
<div class="footer">
  <p >Created by Khramiankova Katsiaryna</p>
</div>




</body>
</html>