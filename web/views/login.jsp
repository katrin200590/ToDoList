<%--
  Created by IntelliJ IDEA.
  User: XXX
  Date: 05.02.2018
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="by.gsu.epam.model.utils.Constants" %>
<html>
<head>
    <title>Log in</title>



    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/reset.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/layout.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login-page.css" type="text/css">
</head>
<body>

<div class="header">
    <div class="logo-image">
        <img src="${pageContext.request.contextPath}/image/to-do-list-logo.png">
        <h2 class="logo-text">My ToDoList</h2>
    </div>
    <c:if test="${not empty errMessage}">
        <div class="err_message"><c:out value="${errMessage}" /></div>
    </c:if>
    <div class="menu">
        <a href="${pageContext.request.contextPath}/">Main</a>
        <p class="menu-selected">Log in</p>
        <a href="startSingup">Sign up</a>




    </div>


</div>


<div class="content">

    <div class="content-text">
        <c:if test="${not empty errMessage}">
            <div class="err_message"><c:out value="${errMessage}" /></div>
        </c:if>
        <div class="login-area">
            <h3>Enter your login and password  </h3>

            <form name="login-form" method="POST" action="logIn">
                <p>Login:</p>

                    <input type="text" name=<%= Constants.KEY_LOGIN %> >

                <p>Password:</p>

                    <input type="password" name=<%= Constants.KEY_PASSWORD %> >

                <input class="input-button" type="submit" value="Log in">

            </form>


        </div>
<div class="bottom-label">
        <H1 >Don't have an account?</H1>
        <a href="startSingup">Sign up</a>
</div>
    </div>
    <img class="big-image" src="${pageContext.request.contextPath}/image/main-page.png">

</div>
<div class="footer">
    <p >Created by Khramiankova Katsiaryna</p>
</div>










</body>

</html>
